import * as path from 'path';
import { Complex, JuliaSet } from './julia';
import { Palette, ColorPoint, Color } from './palette';
import { JuliaSetVisualizer, Size, Animator } from './rendering';

async function main () {
    const c = new Complex(-0.78, 0.136);
    const juliaSet = new JuliaSet(c);
    const palette = new Palette([
        new ColorPoint(0.00, new Color(0x13, 0x2f, 0xff)),
        new ColorPoint(0.15, new Color(0x32, 0x76, 0xff)),
        new ColorPoint(0.30, new Color(0x32, 0xdd, 0xff)),
        new ColorPoint(0.45, new Color(0x00, 0xe3, 0x78)),
        new ColorPoint(0.60, new Color(0xd2, 0xdf, 0x00)),
        new ColorPoint(0.75, new Color(0xdf, 0x7a, 0x00)),
        new ColorPoint(0.90, new Color(0xff, 0x11, 0x56)),
        new ColorPoint(0.97, new Color(0xcd, 0x00, 0xb7)),
        new ColorPoint(1.00, new Color(0, 0, 0))
    ]);

    const visualizer = new JuliaSetVisualizer(juliaSet, palette);
    const center = new Complex(-0.00280967151194, 0.003064073756569);
    const imageSize = new Size(1280, 720);

    const animator = new Animator(visualizer, imageSize);
    await animator.generateImageSequence(center, path.resolve('../sequence'));

    console.log('Done');
}

main();
