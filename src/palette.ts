class Color {
    public constructor(public r: number, public g: number, public b: number) {
        if (!Color.isInRange(r) || !Color.isInRange(g) || !Color.isInRange(b)) {
            throw new Error('Expected all components of the color to be in the range [0, 255]');
        }
    }

    private static isInRange(c: number) : boolean {
        return (c >= 0) && (c <= 255);
    }
}

/**
 * A color point defines a specific color together with a "stop" within a continuous
 * gradient from 0 to 1. It is used to interpolate colours in such gradient when all stops are defined.
 */
class ColorPoint {
    public constructor(public point: number, public color: Color) { 
        if ((point < 0) || (point > 1)) {
            throw new Error('Expected point to be in the interval [0, 1]');
        }
    }
}

/**
 * Represents a palette of any number of color points used in rendering. Imagine this as a
 * continuous gradient of colors painted on a strip, where one edge of the strip represents the
 * stop labeled "0", and the other edge the stop labeled "1". In between any number of stops
 * can be fixed, with a given color associated to them.
 */
class Palette {
    public constructor (private stops: Array<ColorPoint>) { }

    /**
     * Interpolate the color at the given point in this palette. The rules used to obtain the
     * final colors are the following:
     *   - if alpha <= 0, the color in the first stop is returned;
     *   - if alpha >= 1, the color in the last stop is returned;
     *   - otherwise, if t <= alpha < k, the linear interpolation between the color at
     *     stop t and the color at stop k is returned.
     * @param alpha Any number between 0 and 1.
     */
    public interpolate (alpha: number) : Color {
        if (alpha <= 0)
            return this.stops[0].color;

        if (alpha >= 1)
            return this.stops[this.stops.length - 1].color;

        for (let i = 1; i < this.stops.length; ++i) {
            const s0 = this.stops[i - 1];
            const s1 = this.stops[i];
            const t0 = s0.point;
            const t1 = s1.point;

            if ((alpha >= t0) && (alpha < t1))
                return Palette.lerp(s0.color, s1.color, (alpha - t0) / (t1 - t0));
        }
    }

    /**
     * Linearly interpolate between color0 and color1 with the given coefficient alpha.
     * @param alpha Any number between 0 and 1.
     */
    public static lerp (color0: Color, color1: Color, alpha: number) : Color {
        return new Color(
            Math.floor(color0.r + (color1.r - color0.r) * alpha),
            Math.floor(color0.g + (color1.g - color0.g) * alpha),
            Math.floor(color0.b + (color1.b - color0.b) * alpha));
    }
}

export { Color, ColorPoint, Palette };