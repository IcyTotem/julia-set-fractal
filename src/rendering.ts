import * as Jimp from 'jimp';
import * as path from 'path';

import { Complex, JuliaSet } from './julia';
import { Palette } from './palette';

class Size {
    public constructor (public width: number, public height: number) {
        if ((width <= 0) || (height <= 0)) {
            throw new Error('Expected both with and height to be positive non-zero numbers');
        }
    }
}

/**
 * This is used to render a julia set onto an image with the given palette.
 */
class JuliaSetVisualizer {
    public constructor (private juliaSet: JuliaSet, private palette: Palette) { }

    private async createEmptyImage (width: number, height: number) : Promise<any> {
        return new Promise((resolve, reject) => {
            new Jimp(width, height, (err: any, image: any) => err ? reject(err) : resolve(image));
        });
    }

    /**
     * Generate a visualization of the julia set with the palette given in the constructor.
     * @param center Complex point that is rendered at the center of the image. This indirectly defines where 
     *     visualizer is looking within the fractal. Using different centers you can render different sections of it.
     * @param imageSize Resolution of the final image. Virtually unbounded: just remember it has to fit in your working memory.
     * @param zoom Level of zoom to use for rendering. Sensible values range from 1 to 10^7. Beyond the latter,
     *     default arithmetic implementation is not precise enough. If you want to go depper, you'll need to replace
     *     javascript numbers with an alternative implementation of arbitrary-precision floating points (which is generally very slow).
     * @param outputFileName Path or file name in which to save the output. Must contain a valid image extension (png or jpg).
     */
    public async generateImage (center: Complex, imageSize: Size, zoom: number, outputFileName: string) : Promise<any> {
        if (zoom < 1) {
            throw new Error('Zoom level must be at least 1');
        }

        if (!outputFileName.endsWith('.jpg') && !outputFileName.endsWith('.png')) {
            throw new Error('Invalid output file name extension. Allowed extensions are: .png, .jpg');
        }

        const image = await this.createEmptyImage(imageSize.width, imageSize.height);

        const DELTA = 1 / zoom;
        const ASPECT_RATIO = imageSize.width / imageSize.height;
        const R_MIN = center.re - DELTA * ASPECT_RATIO;
        const R_MAX = center.re + DELTA * ASPECT_RATIO;
        const I_MIN = center.im - DELTA;
        const I_MAX = center.im + DELTA;

        for (let x = 0; x < imageSize.width; ++x) {
            for (let y = 0; y < imageSize.height; ++y) {
                const r = R_MIN + (R_MAX - R_MIN) * (x / imageSize.width);
                const i = I_MIN + (I_MAX - I_MIN) * (y / imageSize.height);
                const convergence = this.juliaSet.computeConvergence(new Complex(r, i));
                const color = this.palette.interpolate(Math.sqrt(convergence));
                image.setPixelColor(Jimp.rgbaToInt(color.r, color.g, color.b, 255, undefined), x, y);
            }
        }

        return image.write(outputFileName);
    }
}

/**
 * The animator uses a visualizer to render the same julia set centered on the same fixed point
 * at multiple levels of zoom so that the output can be combined into a video.
 * This class does not create a video. It only creates the sequence of images that you can use
 * to encode a video via an external program such as ffmpeg.
 */
class Animator {
    /**
     * Desired number of frames per seconds for the output. Notice that this value should be the
     * same as the frame rate used to encode the video.
     */
    public frameRate: number = 30;

    /**
     * Desired duration, in seconds, of the final animation. Again, this only influences the number
     * and speed with which the animator goes from the starting zoom to the final zoom. You have
     * to use the same frame rate in your video encoder of choice to make this parameter matter.
     */
    public animationDuration: number = 30;

    /**
     * Initial zoom of the sequence.
     */
    public startingZoom: number = 1;

    /**
     * Final zoom of the sequence. Zoom will increase exponentially through time.
     */
    public finalZoom: number = 1e+6;

    /**
     * Desired image format for produced images.
     */
    public format: 'png' | 'jpg' = 'png';

    /**
     * Whether the debug output for showing progress is enabled.
     */
    public progressLogEnabled: boolean = true;

    public constructor (private visualizer: JuliaSetVisualizer, private imageSize: Size) { }

    /**
     * Generate a sequence of images to render the zoom animation using the julia set visualizer and
     * resolution given in the constructor.
     * @param center Complex number that is used as the center of rendering.
     * @param outputDirectory Name or path of the directory where images will be saved (in the format "out0000.ext").
     */
    public async generateImageSequence (center: Complex, outputDirectory: string) : Promise<void> {
        const logStartingZoom = Math.log10(this.startingZoom);
        const logFinalZoom = Math.log10(this.finalZoom);
        const totalFrames = this.animationDuration * this.frameRate;

        // This is just a soft limitation. You can go beyond that if you want. Just remember to change
        // the output format to pad to 5 characters and be prepared to wait for a while
        if (totalFrames > 9999) {
            throw new Error('Maximum number of frames exceeded');
        }

        for (let i = 0; i < totalFrames; ++i) {
            const alpha = i / totalFrames;
            const logCurrentZoom = logStartingZoom * (1 - alpha) + logFinalZoom * alpha;
            const currentZoom = Math.pow(10, logCurrentZoom);
            const outputFileName = path.resolve(outputDirectory, `out${i.toString().padStart(4, '0')}.${this.format}`);

            await this.visualizer.generateImage(center, this.imageSize, currentZoom, outputFileName);
            
            if (this.progressLogEnabled) {
                console.log(`Progress: ${(alpha * 100).toFixed(2)}%`);
            }
        }
    }
}

export { Size, JuliaSetVisualizer, Animator };