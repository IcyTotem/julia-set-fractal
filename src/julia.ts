class Complex {
    public constructor (public re: number, public im: number) { }
}

/**
 * Represents the JuliaSet for the given value of c passed to its constructor.
 */
class JuliaSet {
    private static ITERATION_LIMIT: number = 128;
    private static VALUE_THRESHOLD: number = 4;

    public constructor (private c: Complex) { }

    /**
     * Compute the converge speed of the given point z with respect to the Julia set quadratic form.
     * @returns A real number between 0 and 1. Low numbers near 0 indicate that z causes the series
     *     to diverge really fast. High numbers near 1 indicate that z causes the series to either
     *     diverge really slowly or not at all (although it is not possible to know which clause
     *     is true in a finite number of steps).
     */
    public computeConvergence (z: Complex) : number {
        let i = 0;
        let c = this.c;

        let zrsqr = z.re * z.re;
        let zisqr = z.im * z.im;

        // This is an optimized algorithm to compute z^2 + c that operates directly on the
        // real an imaginary parts of z and c using the least amount of sums and multiplications.
        // It's useful to avoid the usage of Math.pow as it is a lot slower than this technique

        while (zrsqr + zisqr <= JuliaSet.VALUE_THRESHOLD) {
            z.im = z.re * z.im;
            z.im += z.im; // Multiply by two
            z.im += c.im;
            z.re = zrsqr - zisqr + c.re;
            zrsqr = z.re * z.re;
            zisqr = z.im * z.im;

            if (++i > JuliaSet.ITERATION_LIMIT)
                break;
        }

        return Math.min(i / JuliaSet.ITERATION_LIMIT, 1.0);
    }
}

export { Complex, JuliaSet };