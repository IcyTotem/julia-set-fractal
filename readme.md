## Julia set visualizer

This is just a small toy project to visualize the Julia set of the quadratic form `f(z) = z^2 + c`. 

In order to run the code, you need to have the typescript compiler. Configuration is already provided in `tsconfig.json`. Then run `npm install` to get the necessary packages.

### Demo

Run `npm run demo` to start the demo. It will create a sequence of renderings of a specific Julia set with increasing levels for zoom, and save them in `./sequence`. Then you can run `./generate-video.sh` to create a zooming animation from that sequence. In order for it to work you will need `ffmpeg` installed on your machine and available in your `PATH`.

![First snapshot in the sequence](sequence/out0000.png)

You can edit the values in `demo.ts` to get different results. If instead of a video you simply want an image, remove the animator code and just use the visualizer:

```js
const myZoom = 3.42e+5;
await visualizer.generateImage(center, imageSize, myZoom, 'my-output.png');
```